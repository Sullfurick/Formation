Clear-Host

<#

 This is a big commentary

#>

#This is a small commentary

[string]$personName = "Gaspard"
[string]$personAge = 46
[Int32]$personDoubleAge = [Convert]::ToInt32($personAge, 10) * 2

Write-Host "Hello $personName ! You are $personAge years old. Double it and you'll be $personDoubleAge years old !"

switch($personAge)
{
    {$_ -le 25} {"{0}, you are {1} years old. You are young, GG !" -F $personName, $personAge;break}
    {$_ -le 35} {"{0}, you are {1} years old. You are becoming old... Sorry for you !" -F $personName, $personAge;break}
    {$_ -le 45} {"{0}, you are {1} years old. You have to care about cancer and diseases :(." -F $personName, $personAge;break}
    default {"{0}, you are {1} years old. You are old. Death is near." -F $personName, $personAge}
}
Write-Host "Thanks for passing by !"

if ($personAge -le 25) {Write-Host "You are young, GG !"}

elseif ($personAge -le 35) {Write-Host "You are becoming old... Sorry for you !"}

elseif($personAge -le 45) {Write-Host "You have to care about cancer and diseases :(."}

else{Write-Host "You are old. Death is near."}

