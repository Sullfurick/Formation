function Stagiaire {
    [CMDletBinding()]
    param (
        [string]$personName,
        [Int32]$personAge
    )
    BEGIN {
        Write-Verbose "Script beginning"
    }
    
    PROCESS {
        "Hello {0} ! You are {1} years old." -F $personName, $personAge
    }
    END {
        Write-Verbose "Script ending"
    }
}


Stagiaire "Gaspard" 67
Stagiaire "Bob" 27 -Verbose