Clear-Host
############# DECLARATION VARIABLES/TABLEAUX #############
$cities = "Rennes", "Guingamp", "Grâces", "Pommerit-Le-Vicomte"
$cities += "Saint-Grégoire"
$cities += "Le Merzer"


"Il y a {0} élements dans le tableau Villes" -F ($cities.Count)

$cities[1]

$countries = "France", "Allemagne", "Norvège", "Nouvelle-Zélande"

"Il y a {0} élement(s) dans le tableau Pays" -F ($countries.Count)

##################### BOUCLE FOR #####################

for ($i = 0; $i -lt $cities.Count, $i++) 
{
    $cities[$i]
}

##################### BOUCLE FOREACH #####################

foreach ($city in $cities) 
{
    "{0} is a very beautiful city !" -F $city
}